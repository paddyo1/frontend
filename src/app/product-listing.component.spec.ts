/* tslint:disable:no-unused-variable */

import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { Product } from './product';
import { ProductsComponent } from './products.component';
import { ProductListingComponent } from './product-listing.component';
import { ProductService } from './product.service';

describe('ProductListingComponent', () => {

	const fakeProducts:Product[] = [
		{ name: 'a', rate: 0.01 },
		{ name: 'b', rate: 0.02 }
	];
	let component;
	let fixture;
	let productService:ProductService;

	class ProductServiceSpy {

		getProducts() {
			return Promise.resolve(fakeProducts);
		}

	}

	beforeEach(async(() => {
		TestBed.configureTestingModule({
      declarations: [ ProductListingComponent, ProductsComponent ],
			providers: [ { provide: ProductService, useClass: ProductServiceSpy } ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductListingComponent);
		component = fixture.componentInstance;
		productService = fixture.debugElement.injector.get(ProductService);
	});

	it('lists the products returned from the product service', fakeAsync(() => {
		fixture.detectChanges();
		tick();
		fixture.detectChanges();
		let productElements = fixture.debugElement.queryAll(By.css('.products-list li'));
		expect(productElements.length).toBe(2);
	}));

	//it('displays the name');
	//it('displays the rate');

});
