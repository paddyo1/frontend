import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Product } from './product';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProductService {

	private headers = new Headers({'Content-Type': 'application/json'});
	private url = 'http://localhost:8080/api/products'; //@TODO Base URL TO CONFIG

	constructor(private http:Http) {}

	getProducts(limit?:number): Promise<Product[]> {
		return this.http.get(this.url)
			.toPromise()
			.then(response => response.json() as Product[])
			.then(products => limit ? products.slice(0, limit) : products);
	}

}
