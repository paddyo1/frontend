import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'product-listing',
  templateUrl: './product-listing.component.html'
})
export class ProductListingComponent implements OnInit {

	products: Product[];

	constructor(private productService: ProductService) {}

	getProducts(): void {
		this.productService.getProducts()
			.then(products => this.products = products);
	}

	ngOnInit(): void {
		this.getProducts();
	}

}
