import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

	products: Product[];

	constructor(private productService: ProductService) {}

	getProducts(): void {
		this.productService.getProducts(3)
			.then(products => this.products = products);
	}

	ngOnInit(): void {
		this.getProducts();
	}

}
