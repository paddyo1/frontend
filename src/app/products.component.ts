import { Component, Input } from '@angular/core';
import { Product } from './product';

@Component({
	selector: 'products',
	templateUrl: './products.component.html'
})
export class ProductsComponent {

	@Input()
	products: Product[];

}
