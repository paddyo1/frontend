import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { ProductsComponent } from './products.component';
import { ProductListingComponent } from './product-listing.component';
import { ProductService } from './product.service';
import { RoutingModule } from './routing.module';

@NgModule({
  declarations: [
    AppComponent,
		HomeComponent,
		ProductsComponent,
		ProductListingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
		RoutingModule
  ],
  providers: [ ProductService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
